﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boolean_ChangingAVariable
{
    class Program
    {
        static void Main(string[] args)
        {
            //Storage of variables
            var user = "0";
            var answer = "0";
            var empathy = "0";

            //Getting the users name
            Console.WriteLine("Hi there, what's you're name?");
            user = Console.ReadLine();
            //Getting information from the user
            Console.WriteLine($"Nice to meet you {user}, here how this program works");
            Console.WriteLine("This is going to give you a true or false question and response if you get it right or wrong");
            //Asking the question
            Console.WriteLine("How many times larger than 5! is 7!?");
            answer = Console.ReadLine();

            //If statement to see if it's right
            if (answer == "42" || answer == "42 times")
            {
                answer = "correct";
                empathy = "Congratulations";
            }
            else
            {
                answer = "wrong";
                empathy = "Bummer";
            }
            //Outputs the saved variables
            Console.WriteLine($"{empathy} {user} you got it {answer}");
        }
    }
}
